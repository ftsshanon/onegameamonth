var canvas			= document.getElementById('canvas');
canvas.width		= document.body.clientWidth;
canvas.height		= document.body.clientHeight;
canvas.style.width	= canvas.width + "px";
canvas.style.height = canvas.height + "px";
var ctx 			= canvas.getContext('2d');
input.offset = new Vector2(GetLeft(canvas), GetTop(canvas));
var player = new Player();
var offsetXStart = new Rectangle(canvas.width / 2, 0, canvas.width, canvas.height);
var offsetYStart = new Rectangle(0, canvas.height / 2, canvas.width, 1);
offsetX = 0;
offsetY = 0;
x = offsetX;
y = offsetY;
var Update = setInterval(function() {
	player.Update();


	
}, 1);

var Draw = setInterval(function() {
	ctx.save();
    ctx.translate(offsetX, offsetY);
	
	ctx.clearRect(-offsetX, -offsetY, canvas.width, canvas.height);
	ctx.fillText("Player X: " + (player.hitbox.x + player.hitbox.width) / 2 + ", Player Y:" + (player.hitbox.y + player.hitbox.height) / 2, -offsetX + 10, -offsetY + 10);
	ctx.fillText("xOffset: " + offsetX + ", yOffset:" + offsetY, -offsetX + 10, -offsetY + 20);
	player.Draw(ctx);
	ctx.restore();
}, 33); 

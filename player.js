Player = function() {
	this.hitbox = new Rectangle(canvas.width / 2 - 32, canvas.height / 2 - 32, 64, 64);
	this.animation = new Animation(32, 32, 0, 0, 3, "sprite_sheet.png", 12, 32, 32, 2, 2);
	
	this.moving = false;
	this.SetPosition = function(x, y, mod) {
		if(mod == null || !mod) {
			if(x != null) 
				offsetX = x;
			if(y != null)
				offsetY = y
		}
		else {
			if(x != null)
				offsetX += x;
			if(y != null)
				offsetY += y;
		}
	}
	
	this.Update = function() {
		moving = false;
		
		if(input.right) {
			this.animation.SetRow(1);
			offsetX -= 1;
			this.hitbox.x += 1;
			this.moving = true;
			this.animation.Update();
			
		}
		else if(input.left) {
			this.animation.SetRow(0);
			offsetX += 1;
			this.hitbox.x -= 1;
			this.moving = true;
			this.animation.Update();
		}
		else if(input.up) {
			this.animation.SetRow(3);
			offsetY += 1;
			this.hitbox.y -= 1;
			this.moving = true;
			this.animation.Update();
			
		}
		else if(input.down) {
			this.animation.SetRow(2);
			offsetY -= 1;
			this.hitbox.y += 1;
			this.moving = true;
			this.animation.Update();
		}
		x = offsetX;
		y = offsetY;
		this.animation.position.Set(this.hitbox.x, this.hitbox.y);
		if(!this.moving)
			this.animation.SetColumn(0);
	}
	
	this.Draw = function(ctx) {
		//this.hitbox.Draw(ctx);
		this.animation.Draw(ctx);
	}
};